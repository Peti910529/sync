<?php

require __DIR__ . '/../vendor/autoload.php';

// Run app
$app = (new Pgsimon\Sync\App())->get();
$app->run();
