<?php
namespace Pgsimon\Sync;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\App as SlimApp;

class App
{
    /**
     * Get a new application instance
     *
     * @return SlimApp
     */
    public function get(): SlimApp
    {
        $app = AppFactory::create();

        $app->get('/', function (Request $request, Response $response, $args) {
            $response->getBody()->write("Hello world!");
            return $response;
        });

        return $app;
    }
}
